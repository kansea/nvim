--- This module will load a random colorscheme on nvim startup process.

local utils = require("utils")

local M = {}

-- Colorscheme to its directory name mapping, because colorscheme repo name is not necessarily
-- the same as the colorscheme name itself.
M.colorscheme2dir = {
  onedark = "onedark.nvim",
  onedarkpro = "onedarkpro.nvim",
  papercolor = "papercolor-theme",
}

M.onedark = function()
  require('onedark').setup({
    style = "deep",
    transparent = true,
    term_colors = true,
    ending_tildes = false,
    cmp_itemkind_reverse = false,

    code_style = {
      comments = 'italic',
      keywords = 'none',
      functions = 'bold',
      strings = 'none',
      variables = 'none'
    },

    lualine = {
      transparent = true,
    },

    diagnostics = {
      darker = true,
      undercurl = true,
      background = true,
    },
  })

  vim.cmd([[colorscheme onedark]])
end

M.onedarkpro = function()
  require("onedarkpro").setup({
    styles = {
      comments = "italic",
      methods = "bold",
      functions = "bold",
    },
    options = {
      cursorline = true,
      transparency = true,
    }
  })

  -- set colorscheme after options
  vim.cmd('colorscheme onedark_vivid')
end

M.papercolor = function()
  vim.cmd([[set background=light]])
  vim.cmd('colorscheme PaperColor')
end

M.onedark()
-- M.papercolor()

